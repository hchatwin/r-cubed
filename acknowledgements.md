# Acknowledgements

Content from multiple sources were used as inspiration or
were copied and modified,
including the [R for Data Science] book 
and the [Fundamentals of Data Visualization].
Many pieces from the (in development) course [Merely Useful](https://github.com/merely-useful/merely-useful.github.io)
were modified and used in many places throughout the r-cubed course.
Other parts were modified from:

- The [Aarhus University Community of Research Software Users] sessions on
[project management](https://au-cru.github.io/content/workflow-setup.html),
[data management and wrangling](https://au-cru.github.io/content/intro-dplyr.html),
[intermediate dplyr and tidyr usage](https://au-cru.github.io/site/material/2020-01-17-intermediate-dplyr/),
[data visualization](https://au-cru.github.io/content/intro-ggplot2.html),
and [R Markdown](https://au-cru.github.io/content/intro-rmarkdown.html).

- [A Git session](https://uoftcoders.github.io/studyGroup/lessons/git/intro/lesson/),
given for the [UofTCoders](https://uoftcoders.github.io/studyGroup/).
- The [UofTCoders Intro to R course for Ecology students](https://uoftcoders.github.io/rcourse/).

[Aarhus University Community of Research Software Users]: https://au-cru.github.io/
[Fundamentals of Data Visualization]: https://serialmentor.com/dataviz/
[R for Data Science]: https://r4ds.had.co.nz/
